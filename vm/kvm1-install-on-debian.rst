### KVM 1：Debian Host

服务器上只在Debian Host上运行Docker，虽然images可以建很多，但只适合跑Docker，单纯Linux系统有时也需要，考虑多种应用场景，还应该用VM。

之前用Debian+VirtualBox+Vagrant+Docker有两个问题，一是VirtualBox现在属于Oracle，另外，遇到Dockerfile在VM里报错。

现在设想建KVM VM+Docker，Debian 8 Host，KVM + QEMU VM，两种VMs，VM_L里面运行Docker，VM_S做服务器VM。


下载64位Debian8.2，从大连理工服务器下载。路径：

http://mirror.dlut.edu.cn/debian-cdimage/current/amd64/iso-cd/debian-8.2.0-amd64-CD-1.iso

// Rufus制作Bootable USB Flash

Rufus打开，从镜像选项选ISO，打开图标，找到iso文件，开始复制，从制作的这个USB启动。注意设置BIOS启动顺序。

// 安装Debian

从USB启动，开始安装。

// Partition Disk
// Select "Guided entire disk with LVM"

// None-free repo? Yes
// Security Repo? No

// Software selections
// "Standard System utilities" yes, others no.


//First time boot
// commment out CD-Rom
// sudo nano /etc/apt/sources.list
sudo apt-get update
sudo apt-get install openssh-server vim

// Host Static IP
// edit /etc/network/interfaces
####################################
iface eth0 inet static

address 192.168.1.111
netmask 255.255.255.0
network 192.168.1.0
broadcast 192.168.1.255
gateway 192.168.1.1
######################################

// Install frimware-realtek
// 网卡驱动，Missing firmware: rtl_nic/rtl8168g-2.fw
// non-free repo openned
sudo apt-get install firmware-linux-nonfree
sudo apt-get install frimware-realtek 


// Set root password

$ sudo passwd root
// input password next
$ su 

// Install desktop
// 需要人工启动桌面startx。
$ apt-get install xorg lxde-core

// Install Chinese Font
// 安装开源文泉驿中文字体
$ sudo apt-get install ttf-wqy-zenhei

// 安装浏览器, 安装Firefox，通过Email附件下载Google Chrome Deb文件
// 安装方法参见相关文档。

// Tune Termial Emulator

## Make Hotkey for starting LXTerminal

// CTRL+ALT+T open LXTerminal

// Very good Tutorial:
// **http://linux-commands.wikidot.com/howto-s:lxde

$ sudo nano ~/.config/openbox/lxde-rc.xml

// C-A-t 启动/usr/bin/x-terminal-emulator
// C-A-W-i 启动/usr/bin/firefox

// In section <!— Keybindings for running applications —> add this…

<keybind key="A-C-t">
<action name="Execute">
<command>/usr/bin/x-terminal-emulator</command>
</action>
</keybind>

<keybind key="A-C-W-i">
<action name="Execute">
<command>/usr/bin/firefox</command>
</action>
</keybind>

// Note:

S = Shift
C = Control
A = Alt
W = Super key (Win)

// Refresh settings
$ openbox --reconfigure

// Restart LXDE panel
$ lxpanelctl restart

## Xterm Terminal font size too small

# Create a new file, with regular user
$ sudo nano ~/.Xresources

# Add these lines:

! xterm ----------------------------------------------------------------------


xterm*VT100.geometry: 80x25
xterm*faceName: Terminus:style=Regular:size=12
!xterm*font: -*-dina-medium-r-*-*-16-*-*-*-*-*-*-*
xterm*dynamicColors: true
xterm*utf8: 2
xterm*eightBitInput: true
xterm*saveLines: 512
xterm*scrollKey: true
xterm*scrollTtyOutput: false
xterm*scrollBar: true
xterm*rightScrollBar: true
xterm*jumpScroll: true
xterm*multiScroll: true
xterm*toolBar: false

$ sudo reboot